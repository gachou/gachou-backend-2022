package org.knappi.gachou.rest_api.endpoints.examples;

import org.eclipse.microprofile.openapi.annotations.Operation;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

// Will be removed when there are other admin resources
@Path("/api/admin")
@RolesAllowed("admin")
public class AdminResource {

    @Operation(operationId = "adminExample",
               description = "Example for restricted resource")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String admin() {
        return "granted";
    }
}