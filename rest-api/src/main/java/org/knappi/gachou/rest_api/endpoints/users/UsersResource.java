package org.knappi.gachou.rest_api.endpoints.users;

import io.quarkus.security.identity.SecurityIdentity;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.jboss.resteasy.reactive.NoCache;
import org.knappi.gachou.rest_api.endpoints.users.dto.UserResponse;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/api/users")
@Produces(MediaType.APPLICATION_JSON)
@PermitAll
public class UsersResource {

    @Inject
    SecurityIdentity identity;

    @Operation(operationId = "usersMe",
               description = "Returns data about the currently logged in user")
    @GET
    @Path("/me")
    @NoCache
    public UserResponse me() {
        return UserResponse.builder().username(identity.getPrincipal().getName()).build();
    }

}