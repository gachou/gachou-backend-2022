package org.knappi.gachou.rest_api.endpoints.devtest;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.knappi.gachou.core.usecases.devtest.TestSetupService;
import org.knappi.gachou.rest_api.endpoints.devtest.mapper.TestSetupMapper;
import org.knappi.gachou.rest_api.endpoints.devtest.model.TestSetupRequest;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Optional;

/**
 * This endpoint can be used to set up test-data for integration tests.
 * <p>
 * This is not possible directly from the test, because the test does not run
 * in a container and the application has been compiled before.
 * <p>
 * The endpoint requires a special header with an authentication token
 * ({@link DangerousTestSetupProperties}, which define via a config option.
 * If the token is not specified in the config, this endpoint is not
 * <p>
 * Because it would be harmful to have this feature on a production environment, as an
 * additional security measure, it requires the correct token value to be passed to the
 * test-setup endpoint in the request-headers.
 */
@Produces(MediaType.TEXT_PLAIN)
@Consumes(MediaType.APPLICATION_JSON)
@Path(TestSetupResource.PATH)
@PermitAll
public class TestSetupResource {

    public static final String PATH = "/devtest/reset-gachou-for-tests";

    @ConfigProperty(name = DangerousTestSetupProperties.TOKEN)
    public Optional<String> expectedAccessToken;

    @Inject
    TestSetupService testSetupService;

    @Inject
    TestSetupMapper testSetupMapper;

    @Operation(operationId = "setupTestData",
               description = "Setup test data for integration tests")
    @POST
    public Response resetGachou(@HeaderParam(DangerousTestSetupProperties.TOKEN_HEADER_NAME) String accessToken,
                                @RequestBody TestSetupRequest testSetupRequest) {
        if (expectedAccessToken.isEmpty()) {
            return Response.serverError().entity("Testing endpoint disabled").build();
        }
        if (!expectedAccessToken.get().equals(accessToken)) {
            return Response.status(403).build();
        }

        testSetupService.setupForTest(testSetupMapper.toCore(testSetupRequest));
        return Response.accepted().build();
    }
}