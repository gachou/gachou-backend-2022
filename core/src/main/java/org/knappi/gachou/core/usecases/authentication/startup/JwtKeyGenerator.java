package org.knappi.gachou.core.usecases.authentication.startup;

import io.quarkus.runtime.StartupEvent;
import io.smallrye.jwt.algorithm.SignatureAlgorithm;
import io.smallrye.jwt.util.KeyUtils;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jetbrains.annotations.NotNull;
import org.jose4j.jwk.JsonWebKey;
import org.jose4j.lang.JoseException;

import javax.crypto.SecretKey;
import javax.enterprise.event.Observes;
import javax.inject.Singleton;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.InvalidAlgorithmParameterException;
import java.util.Optional;

@Singleton
@Slf4j
class JwtKeyGenerator {

    @ConfigProperty(name = "gachou.authentication.jwt.key.path")
    Optional<String> jwtKeyPathConfigOption;

    void onStart(@Observes StartupEvent ev) {
        jwtKeyPathConfigOption.ifPresentOrElse(
                this::lazyCreateJwtKey,
                () -> log.warn("gachou.authentication.jwt.key.path is not configured. " +
                        "Skipping key creation")
        );
    }

    private void lazyCreateJwtKey(String jwtKeyPathAsString) {
        Path jwtKeyPath = Path.of(jwtKeyPathAsString);
        if (Files.exists(jwtKeyPath)) {
            log.info("Existing JWT key found at " + jwtKeyPath.toAbsolutePath());
            return;
        }
        try {
            log.info("Creating JWT key at " + jwtKeyPath.toAbsolutePath());
            JsonWebKey jsonWebKey = createJwtKey();
            writeJwtKey(jwtKeyPath, jsonWebKey);
        } catch (InvalidAlgorithmParameterException | JoseException | IOException e) {
            throw new RuntimeException("Error while creating JWT key", e);
        }
    }

    @NotNull
    private JsonWebKey createJwtKey() throws InvalidAlgorithmParameterException, JoseException {
        SecretKey secretKey = KeyUtils.generateSecretKey(SignatureAlgorithm.HS256);
        JsonWebKey jsonWebKey = JsonWebKey.Factory.newJwk(secretKey);
        jsonWebKey.setAlgorithm("HS256");
        return jsonWebKey;
    }

    private void writeJwtKey(Path jwtKeyPath, JsonWebKey jsonWebKey) throws IOException {
        Files.createDirectories(jwtKeyPath.getParent());
        Files.writeString(jwtKeyPath, jsonWebKey.toJson());
    }

}
