package org.knappi.gachou.core.usecases.authentication.exceptions;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
public class UserAlreadyExistsException extends RuntimeException {
    private final String username;

    public UserAlreadyExistsException(String username) {
        super(username + " already exists!");
        this.username = username;
    }

}
