package org.knappi.gachou.core.usecases.authentication;

import io.smallrye.jwt.build.Jwt;
import lombok.RequiredArgsConstructor;
import org.knappi.gachou.core.usecases.authentication.gateway.UserAccounts;

import javax.inject.Singleton;
import javax.naming.AuthenticationException;

@Singleton
@RequiredArgsConstructor
public class LoginService {

    private final UserAccounts userAccounts;

    public String tryCreateJwt(String username, char[] password) throws AuthenticationException {
        if (userAccounts.verifyPassword(username, password)) {
            return Jwt.upn(username).sign();
        } else {
            throw new AuthenticationException("Wrong username or password");
        }
    }
}
