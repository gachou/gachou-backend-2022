package org.knappi.gachou.core.usecases.authentication;

import lombok.RequiredArgsConstructor;
import org.knappi.gachou.core.usecases.authentication.gateway.UserAccounts;

import javax.inject.Singleton;

@Singleton
@RequiredArgsConstructor
public class UserService {

    private final UserAccounts userAccounts;


    public void addUser(String username, char[] password){
        userAccounts.addUser(username, password);
    }

    public void upsertUser(String username, char[] password) {
        userAccounts.upsertUser(username, password);
    }

}
