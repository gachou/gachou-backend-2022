package org.knappi.gachou.testutils;

import org.knappi.gachou.rest_api.endpoints.devtest.DangerousTestSetupProperties;
import org.knappi.gachou.rest_api.endpoints.devtest.TestSetupResource;
import org.knappi.gachou.rest_api.endpoints.devtest.model.TestSetupRequest;
import org.knappi.gachou.rest_api.endpoints.devtest.model.TestUserDto;

import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;

public class TestDataBuilder {

    private final List<TestUserDto> testUsers = new ArrayList<>();

    public static TestDataBuilder startWithWipedData() {
        return new TestDataBuilder();
    }

    public TestDataBuilder addUser(String username, String password) {
        TestUserDto testUser = new TestUserDto();
        testUser.setUsername(username);
        testUser.setPassword(password);
        testUsers.add(testUser);
        return this;

    }

    public void apply() {
        TestSetupRequest request = new TestSetupRequest();
        request.setTestUsers(testUsers);

        given()
                .header(
                        DangerousTestSetupProperties.TOKEN_HEADER_NAME,
                        TestProfileForTestDataSetup.TEST_SETUP_ACCESS_TOKEN
                )
                .body(request)
                .contentType(MediaType.APPLICATION_JSON)
                .when()
                .post(TestSetupResource.PATH)
                .then()
                .assertThat()
                .statusCode(202);
    }

}
