package org.knappi.gachou.testutils;

import io.quarkus.test.junit.QuarkusTestProfile;
import org.knappi.gachou.rest_api.endpoints.devtest.DangerousTestSetupProperties;

import java.util.Map;

public class TestProfileForTestDataSetup implements QuarkusTestProfile {
    public final static String TEST_SETUP_ACCESS_TOKEN = "integration-test-token";

    @Override
    public Map<String, String> getConfigOverrides() {
        return Map.of(
                DangerousTestSetupProperties.TOKEN,
                TEST_SETUP_ACCESS_TOKEN
        );
    }
}
