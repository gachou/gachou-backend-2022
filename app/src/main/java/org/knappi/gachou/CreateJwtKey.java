package org.knappi.gachou;

import io.smallrye.jwt.algorithm.SignatureAlgorithm;
import io.smallrye.jwt.util.KeyUtils;
import lombok.extern.slf4j.Slf4j;
import org.jose4j.base64url.Base64;
import org.jose4j.jwk.JsonWebKey;

import javax.crypto.SecretKey;

@Slf4j
public class CreateJwtKey {
    public static void main(String[] args) throws Exception {
        SecretKey secretKey = KeyUtils.generateSecretKey(SignatureAlgorithm.HS256);
        JsonWebKey jsonWebKey = JsonWebKey.Factory.newJwk(secretKey);
        jsonWebKey.setAlgorithm("HS256");

        System.out.println("JWK: " + jsonWebKey.toJson());
        System.out.println("BASE64: " + Base64.encode(jsonWebKey.toJson().getBytes()));

    }
}
