package org.knappi.gachou.database.authentication.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Optional;

@Entity(name = "user_password")
@Getter
@Setter
public class UserPasswordEntity extends PanacheEntity {

    public static Optional<UserPasswordEntity> findByUsername(String username) {
        return find("username",username).firstResultOptional();
    }

    private String username;

    @Column(name = "password_salt")
    public String salt;

    @Column(name = "password_hash")
    public String hash;

    @Column(name = "password_iteration_count")
    public int iterationCount;
}
