package org.knappi.gachou.database.devtest;

import org.knappi.gachou.core.usecases.devtest.gateway.DangerousDatabaseReset;
import org.knappi.gachou.database.authentication.entity.UserPasswordEntity;

import javax.inject.Singleton;

@Singleton
public class DangerousDatabaseResetGatewayImpl implements DangerousDatabaseReset {

    /**
     * For testing purposes only
     */
    public void wipeData() {
        UserPasswordEntity.deleteAll();
    }
}
