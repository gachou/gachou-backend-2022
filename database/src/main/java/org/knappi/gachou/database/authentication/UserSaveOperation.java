package org.knappi.gachou.database.authentication;


import org.knappi.gachou.core.usecases.authentication.exceptions.UserAlreadyExistsException;
import org.knappi.gachou.database.authentication.entity.UserPasswordEntity;

import java.security.SecureRandom;
import java.util.Optional;

import static org.knappi.gachou.database.authentication.Base64Util.encodeBase64;
import static org.knappi.gachou.database.authentication.UserPasswordGatewayImpl.ITERATIONS;
import static org.knappi.gachou.database.authentication.UserPasswordGatewayImpl.createBcryptHash;

class UserSaveOperation {
    public final String username;
    public final char[] password;

    public UserSaveOperation(String username, char[] password) {
        this.username = username;
        this.password = password;
    }

    public void addUser() {
        if (UserPasswordEntity.findByUsername(username).isPresent()) {
            throw new UserAlreadyExistsException(username);
        }
        UserPasswordEntity userPasswordEntity = newEntity();
        updatePasswordComponents(password, userPasswordEntity);
        userPasswordEntity.persist();
    }

    public void upsertUser() {
        Optional<UserPasswordEntity> userByName = UserPasswordEntity.findByUsername(username);
        UserPasswordEntity userPasswordEntity = userByName.orElseGet(this::newEntity);
        updatePasswordComponents(password, userPasswordEntity);
        userPasswordEntity.persist();
    }

    public UserPasswordEntity newEntity() {
        UserPasswordEntity userPasswordEntity = new UserPasswordEntity();
        userPasswordEntity.setUsername(username);
        return userPasswordEntity;
    }

    private static void updatePasswordComponents(char[] password,
                                                 UserPasswordEntity userPasswordEntity) {
        byte[] salt = createSalt();
        byte[] hash = createBcryptHash(salt, ITERATIONS, password);
        userPasswordEntity.setHash(encodeBase64(hash));
        userPasswordEntity.setSalt(encodeBase64(salt));
        userPasswordEntity.setIterationCount(ITERATIONS);
    }

    private static byte[] createSalt() {
        byte[] salt = new byte[16];
        SecureRandom secureRandom = new SecureRandom();
        secureRandom.nextBytes(salt);
        return salt;
    }
}
