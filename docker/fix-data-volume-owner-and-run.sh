#!/bin/bash

if [ "$#" -lt 1 ] ; then
  echo "Usage: $0 <target-uid> cmd ..."
  echo
  echo "docker-entrypoint that sets the correct owner of the 'gachou-data' volume"
  echo "before switching the the same user and running the 'command'"
  exit 1
fi

TARGET_UID=$1
shift 1

chown -R "${TARGET_UID}:${TARGET_UID}" /gachou-data
exec gosu "${TARGET_UID}" "$@"